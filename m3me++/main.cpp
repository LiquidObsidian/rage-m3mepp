/*
*	m3me++ ragehack by Liquid Obsidian
*
*	<3 from m3mers
*
*	http://liqob.com/m3mers/
*/

#pragma region Initialize

#include <iostream>
#include <thread>
#include "Memory.h"
#include "Offsets.h"
#include "BSP.h"

HANDLE hConsole;
Memory Mem;
Module mClient;
DWORD dwClient;
Module mEngine;
DWORD dwEngine;
Offsets Offset;

using namespace std;

#pragma endregion Initialize

#pragma region BSP

void updateBSP()
{
	int pEngine = Mem.Read<int>(dwEngine + Offset.dwEngine);
	int bInGame = Mem.Read<int>(pEngine + Offset.dwInGame);

	char mapName[32];
	ReadProcessMemory(Mem._proc, (LPCVOID)(pEngine + Offset.dwMapName), &mapName, sizeof(mapName), NULL);

	if (!bInGame)
	{
		if (strcmp(mapName, "") != 0)
		{
			mapName[0] = '\0';
		}
	}
	else
	{
		ReadProcessMemory(Mem._proc, (LPCVOID)(pEngine + Offset.dwMapName), &mapName, sizeof(mapName), NULL);

		if (gBSP->LoadedName != mapName) {
			string path = "D:\\Steam\\Games\\steamapps\\common\\Counter-Strike Global Offensive\\csgo\\maps\\";

			cout << "Loading map.\n";

			if (strcmp(mapName, "")) Sleep(1000 * 15);

			gBSP->LoadBSP(path + mapName + ".bsp");

			ReadProcessMemory(Mem._proc, (LPCVOID)(pEngine + Offset.dwMapName), &mapName, sizeof(mapName), NULL);

			cout << "Loaded " << mapName << ".\n";

			gBSP->LoadedName = mapName;
		}
	}
}

#pragma endregion BSP

#pragma region Engine

struct Angle
{
	float x;
	float y;
	float z;
};

Angle GetBonePosition(DWORD pTargetPlayer, int pBoneID)
{
	Angle pAngle;

	DWORD pBoneMatrix = Mem.Read<int>(pTargetPlayer + Offset.dwBoneMatrix);

	pAngle.x = Mem.Read<float>(pBoneMatrix + 0x30 * pBoneID + 0xC);
	pAngle.y = Mem.Read<float>(pBoneMatrix + 0x30 * pBoneID + 0x1C);
	pAngle.z = Mem.Read<float>(pBoneMatrix + 0x30 * pBoneID + 0x2C);

	return pAngle;
}

Vector3 GetBonePositionVector(DWORD pTargetPlayer, int pBoneID)
{
	Vector3 pVector;

	DWORD pBoneMatrix = Mem.Read<int>(pTargetPlayer + Offset.dwBoneMatrix);

	pVector.x = Mem.Read<float>(pBoneMatrix + 0x30 * pBoneID + 0xC);
	pVector.y = Mem.Read<float>(pBoneMatrix + 0x30 * pBoneID + 0x1C);
	pVector.z = Mem.Read<float>(pBoneMatrix + 0x30 * pBoneID + 0x2C);

	return pVector;
}

Angle NormalizeAngle(Angle pAngle)
{
	if (pAngle.x > 89)
	{
		pAngle.x = 89;
	}
	else if (-89 > pAngle.x)
	{
		pAngle.x = -89;
	}

	if (pAngle.y > 180)
	{
		pAngle.y = pAngle.y - 360;
	}
	else if (-180 > pAngle.y)
	{
		pAngle.y = pAngle.y + 360;
	}

	pAngle.z = 0;

	return pAngle;
}

Angle GetOrigin()
{
	DWORD pLocalPlayer = Mem.Read<int>(dwClient + Offset.dwLocalPlayer);
	Angle pAngle;

	pAngle.x = Mem.Read<float>(pLocalPlayer + Offset.dwVecOrigin);
	pAngle.y = Mem.Read<float>(pLocalPlayer + Offset.dwVecOrigin + 0x4);
	pAngle.z = Mem.Read<float>(pLocalPlayer + Offset.dwVecOrigin + 0x8);;

	return pAngle;
}

Vector3 GetOriginVector()
{
	DWORD pLocalPlayer = Mem.Read<int>(dwClient + Offset.dwLocalPlayer);
	Vector3 pVector;

	pVector.x = Mem.Read<float>(pLocalPlayer + Offset.dwVecOrigin);
	pVector.y = Mem.Read<float>(pLocalPlayer + Offset.dwVecOrigin + 0x4);
	pVector.z = Mem.Read<float>(pLocalPlayer + Offset.dwVecOrigin + 0x8);;

	return pVector;
}

Angle GetPunch()
{
	DWORD pLocalPlayer = Mem.Read<int>(dwClient + Offset.dwLocalPlayer);
	
	Angle pAngle;
	pAngle.x = Mem.Read<float>(pLocalPlayer + Offset.dwVecPunch);
	pAngle.y = Mem.Read<float>(pLocalPlayer + Offset.dwVecPunch + 0x4);
	pAngle.z = 0;

	return pAngle;
}

Angle GetAngles()
{
	DWORD pAnglePointer = Mem.Read<int>(dwEngine + Offset.dwEngine);
	Angle pAngle;

	pAngle.x = Mem.Read<float>(pAnglePointer + Offset.dwViewAngles);
	pAngle.y = Mem.Read<float>(pAnglePointer + Offset.dwViewAngles + 0x4);
	pAngle.z = 0;

	return pAngle;
}

Angle GetEyeAngles()
{
	DWORD pLocalPlayer = Mem.Read<int>(dwClient + Offset.dwLocalPlayer);
	Angle pAngle;

	pAngle.x = Mem.Read<float>(pLocalPlayer + Offset.dwEyeAngles);
	pAngle.y = Mem.Read<float>(pLocalPlayer + Offset.dwEyeAngles + 0x4);
	pAngle.z = 0;

	return pAngle;
}

Angle mAngle(float x, float y, float z)
{
	Angle pAngle;

	pAngle.x = x;
	pAngle.y = y;
	pAngle.z = z;

	return pAngle;
}

Angle GetSmooth(Angle pOriginal, Angle pDest, float Smoothness)
{
	Angle pSmooth;

	pSmooth.x = pDest.x - pOriginal.x;
	pSmooth.y = pDest.y - pOriginal.y;

	NormalizeAngle(pSmooth);

	pSmooth.x = pOriginal.x + pSmooth.x / 100 * Smoothness;
	pSmooth.y = pOriginal.y + pSmooth.y / 100 * Smoothness;
	pSmooth = NormalizeAngle(pSmooth);

	return pSmooth;
}

Angle CalcAngle(Angle pSource, Angle pDestination)
{
	DWORD pLocalPlayer = Mem.Read<int>(dwClient + Offset.dwLocalPlayer);
	Angle pDelta;

	pDelta.x = pSource.x - pDestination.x;
	pDelta.y = pSource.y - pDestination.y;

	DWORD pViewOrigin = Mem.Read<float>(pLocalPlayer + Offset.dwViewOffset + 0x4 + 0x4);

	pDelta.z = pSource.z + pViewOrigin - pDestination.z;

	float siHyp = sqrt(pDelta.x * pDelta.x + pDelta.y * pDelta.y);

	Angle pAngle;
	pAngle.x = atan(pDelta.z / siHyp) * 57.295779513082;
	pAngle.y = atan(pDelta.y / pDelta.x) * 57.295779513082;
	pAngle.z = 0;

	if (pDelta.x >= 0.0)
		pAngle.y += 180;

	pAngle.x = pAngle.x - GetPunch().x * 2;
	pAngle.y = pAngle.y - GetPunch().y * 2;

	return pAngle;
}

float _NormalizeAngle(float pAngle)
{
	return fmod(pAngle + 180, 360) - 180;
}

float AngleDifference(Angle pAngleA, Angle pAngleB)
{
	bool bX180 = false;
	bool bY180 = false;

	float XDiff = _NormalizeAngle(pAngleA.x - pAngleB.x);
	float YDiff = _NormalizeAngle(pAngleA.y - pAngleB.y);

	bX180 = 180 > XDiff;
	bY180 = 180 > YDiff;

	if (!bX180)
		XDiff -= 360;

	if (!bY180)
		YDiff -= 360;

	if (0 > XDiff)
		XDiff = (XDiff - XDiff) - XDiff;

	if (0 > YDiff)
		YDiff = (YDiff - YDiff) - YDiff;

	float Diff = YDiff + XDiff;

	return Diff;
}

void SetAngles(Angle pAngle)
{
	DWORD pAnglePointer = Mem.Read<int>(dwEngine + Offset.dwEngine);

	Mem.WriteNew<float>(pAnglePointer + Offset.dwViewAngles, pAngle.x);
	Mem.WriteNew<float>(pAnglePointer + Offset.dwViewAngles + 0x4, pAngle.y);
}

void SetEyeAngles(Angle pAngle)
{
	DWORD pLocalPlayer = Mem.Read<int>(dwClient + Offset.dwLocalPlayer);

	Mem.WriteNew<float>(pLocalPlayer + Offset.dwEyeAngles, pAngle.x);
	Mem.WriteNew<float>(pLocalPlayer + Offset.dwEyeAngles + 0x4, pAngle.y);
}

DWORD GetClosestPlayerToCross(float FOV)
{
	DWORD pLocalPlayer = Mem.Read<int>(dwClient + Offset.dwLocalPlayer);
	int iLocalPlayerTeam = Mem.Read<int>(pLocalPlayer + Offset.dwTeam);
	
	Angle pAngles = GetAngles();
	Angle pOrigin = GetOrigin();

	int bone = 6;

	DWORD PlayerArray[64];
	float AngleArray[64];

	for (int i = 0; i < 64; i++)
	{
		DWORD pCurPlayer = Mem.Read<DWORD>(dwClient + Offset.dwEntityList + ((i - 1) * 16));

		Angle pAngle = GetBonePosition(pCurPlayer, 6);
		pAngle = CalcAngle(pOrigin, pAngle);
		pAngle = NormalizeAngle(pAngle);

		PlayerArray[i] = pCurPlayer;
		AngleArray[i] = AngleDifference(pAngle, pAngles);
	}

	DWORD ClosestPlayer = 0;
	float ClosestAngle = 360;

	for (int i = 0; i < 64; i++)
	{
		DWORD pPlayer = PlayerArray[i];
		float pAngle = AngleArray[i];

		int iCurPlayerTeam = Mem.Read<int>(pPlayer + Offset.dwTeam);
		bool bDormant = Mem.Read<bool>(pPlayer + Offset.dwDormant);
		bool bAlive = Mem.Read<int>(pPlayer + Offset.dwHealth) > 0;

		Vector3 pOriginVec = GetOriginVector();
		pOriginVec.z += 64;

		bool CanShoot = false;
		if (gBSP->Loaded)
			CanShoot = gBSP->Visible(pOriginVec, GetBonePositionVector(pPlayer, 6));
		else
			CanShoot = true;

		if (!(iCurPlayerTeam == iLocalPlayerTeam) && !bDormant && bAlive && pAngle < ClosestAngle && (FOV >= pAngle) && CanShoot)
		{
			ClosestPlayer = pPlayer;
			ClosestAngle = pAngle;
		}
	}

	return ClosestPlayer;
}

#pragma endregion Engine

#pragma region Threads

DWORD WINAPI aimbot(LPVOID lpParam)
{
	SetConsoleTextAttribute(hConsole, 7);
	cout << "Aimbot thread started.\n";
	
	while (true)
	{
		if (GetAsyncKeyState(VK_XBUTTON2))
		{
			DWORD pLocalPlayer = Mem.Read<int>(dwClient + Offset.dwLocalPlayer);
			DWORD pClosestPlayerToCross = GetClosestPlayerToCross(360);

			SetEyeAngles({
				0,
				0,
				0
			});

			int iTeam = Mem.Read<int>(pLocalPlayer + Offset.dwTeam);

			Angle pAngles = GetAngles();
			Angle pOrigin = GetOrigin();

			int iClosestPlayerTeam = Mem.Read<int>(pClosestPlayerToCross + Offset.dwTeam);

			if (pClosestPlayerToCross > 0)
			{
				Angle pAngle = GetBonePosition(pClosestPlayerToCross, 6);
				pAngle = CalcAngle(pOrigin, pAngle);
				pAngle = NormalizeAngle(pAngle);

				SetAngles(/*GetSmooth(pAngles, */pAngle/*, 5)*/);
				pAngle.y += 30;

				Mem.WriteNew<int>(dwClient + Offset.dwAttack, 1);
				Sleep(1);
				Mem.WriteNew<int>(dwClient + Offset.dwAttack, 4);
			}
		}
		
		Sleep(1);
	}

	return 0;
}

DWORD WINAPI bhop(LPVOID lpParam)
{
	SetConsoleTextAttribute(hConsole, 7);
	cout << "Bhop thread started.\n";

	while (true)
	{
		DWORD pLocalPlayer = Mem.Read<int>(dwClient + Offset.dwLocalPlayer);
		int Flags = Mem.Read<int>(pLocalPlayer + Offset.dwFlags);

		if (GetAsyncKeyState(VK_SPACE) && (Flags & 0x1 == 1))
		{
			Mem.WriteNew<int>(dwClient + Offset.dwJump, 1);
			Sleep(1);
			Mem.WriteNew<int>(dwClient + Offset.dwJump, 4);
		}
		else
		{
			Sleep(1);
		}
	}

	return 0;
}

DWORD WINAPI bspupdate(LPVOID lpParam)
{
	Sleep(1000);

	while (true)
	{
		updateBSP();
		Sleep(1);
	}

	return 0;
}

DWORD WINAPI radar(LPVOID lpParam)
{
	SetConsoleTextAttribute(hConsole, 7);
	cout << "Radar thread started.\n";

	while (true)
	{
		for (int i = 0; i < 64; i++)
		{
			DWORD pCurPlayer = Mem.Read<int>(dwClient + Offset.dwEntityList + ((i - 1) * 16));

			bool pCurPlayerDormant = Mem.Read<bool>(pCurPlayer + Offset.dwDormant);
			bool pCurPlayerSpotted = Mem.Read<bool>(pCurPlayer + Offset.dwSpotted);

			if (!pCurPlayerDormant && !pCurPlayerSpotted)
			{
				Mem.WriteNew<int>(pCurPlayer + Offset.dwSpotted, 1);
			}
		}

		Sleep(15);
	}

	return 0;
}

DWORD WINAPI glow(LPVOID lpParam)
{
	SetConsoleTextAttribute(hConsole, 7);
	cout << "Glow thread started.\n";

	while (true)
	{
		DWORD pLocalPlayer = Mem.Read<int>(dwClient + Offset.dwLocalPlayer);
		int iTeam = Mem.Read<int>(pLocalPlayer + Offset.dwTeam);

		for (int i = 0; i < 64; i++)
		{
			DWORD pCurPlayer = Mem.Read<int>(dwClient + Offset.dwEntityList + ((i - 1) * 16));

			int iCurPlayerTeam = Mem.Read<int>(pCurPlayer + Offset.dwTeam);
			bool bCurPlayerDormant = Mem.Read<bool>(pCurPlayer + Offset.dwDormant);

			int pCurPlayerGlowIndex = Mem.Read<int>(pCurPlayer + Offset.dwGlowIndex);

			int pGlowObj = Mem.Read<int>(dwClient + Offset.dwGlowObject);

			if (bCurPlayerDormant)
				continue;

			if (iTeam == iCurPlayerTeam)
			{
				Mem.WriteNew<float>(pGlowObj + ((pCurPlayerGlowIndex * 0x34) + 0x4), 0);
				Mem.WriteNew<float>(pGlowObj + ((pCurPlayerGlowIndex * 0x34) + 0x8), 0);
				Mem.WriteNew<float>(pGlowObj + ((pCurPlayerGlowIndex * 0x34) + 0xC), 1);
				Mem.WriteNew<float>(pGlowObj + ((pCurPlayerGlowIndex * 0x34) + 0x10), 1);
				Mem.WriteNew<bool>(pGlowObj + ((pCurPlayerGlowIndex * 0x34) + 0x24), true);
				Mem.WriteNew<bool>(pGlowObj + ((pCurPlayerGlowIndex * 0x34) + 0x25), false);
			}
			else 
			{
				Mem.WriteNew<float>(pGlowObj + ((pCurPlayerGlowIndex * 0x34) + 0x4), 1);
				Mem.WriteNew<float>(pGlowObj + ((pCurPlayerGlowIndex * 0x34) + 0x8), 0);
				Mem.WriteNew<float>(pGlowObj + ((pCurPlayerGlowIndex * 0x34) + 0xC), 0);
				Mem.WriteNew<float>(pGlowObj + ((pCurPlayerGlowIndex * 0x34) + 0x10), 1);
				Mem.WriteNew<bool>(pGlowObj + ((pCurPlayerGlowIndex * 0x34) + 0x24), true);
				Mem.WriteNew<bool>(pGlowObj + ((pCurPlayerGlowIndex * 0x34) + 0x25), false);
			}
		}

		Sleep(15);
	}

	return 0;
}

DWORD WINAPI trigger(LPVOID lpParam)
{
	SetConsoleTextAttribute(hConsole, 7);
	cout << "Trigger thread started.\n";

	while (true)
	{
		DWORD pLocalPlayer = Mem.Read<int>(dwClient + Offset.dwLocalPlayer);
		DWORD pInCrossIndex = Mem.Read<int>(pLocalPlayer + Offset.dwInCross);
		DWORD pInCrossPlayer = Mem.Read<int>(dwClient + Offset.dwEntityList + ((pInCrossIndex - 1) * 0x10));

		DWORD iTeam = Mem.Read<int>(pLocalPlayer + Offset.dwTeam);
		DWORD iInCrossTeam = Mem.Read<int>(pInCrossPlayer + Offset.dwTeam);

		if (GetAsyncKeyState(VK_XBUTTON1) && pInCrossIndex > 0 && pInCrossIndex < 65)
		{
			if (iTeam != iInCrossTeam)
			{
				Mem.WriteNew<int>(dwClient + Offset.dwAttack, 1);
				Sleep(1);
				Mem.WriteNew<int>(dwClient + Offset.dwAttack, 4);
			}
		}

		Sleep(1);
	}

	return 0;
}

#pragma endregion Threads

#pragma region Main

int main()
{
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleTextAttribute(hConsole, 11);
	cout << "m3me++ started." << endl;
	SetConsoleTextAttribute(hConsole, 14);
	cout << "Waiting for CS:GO.\n";

	while (!Mem.Attach(L"csgo.exe"))
	{
		Sleep(1000);
	}

	SetConsoleTextAttribute(hConsole, 10);
	cout << "Found CS:GO." << endl << endl;

	mClient = Mem.GetModule(L"client.dll");
	dwClient = mClient.dwBase;

	mEngine = Mem.GetModule(L"engine.dll");
	dwEngine = mEngine.dwBase;

	SetConsoleTextAttribute(hConsole, 8);
	cout << "Client base: ";
	SetConsoleTextAttribute(hConsole, 3);
	cout << "0x" << dwClient << endl;
	SetConsoleTextAttribute(hConsole, 8);
	cout << "Engine base: ";
	SetConsoleTextAttribute(hConsole, 3);
	cout << "0x" << dwEngine << endl << endl;
	
	HANDLE threads[6];
	int i = 1;

	threads[0] = CreateThread(NULL, 0, bhop, &i, 0, NULL);
	i++;
	threads[1] = CreateThread(NULL, 0, trigger, &i, 0, NULL);
	i++;
	threads[2] = CreateThread(NULL, 0, radar, &i, 0, NULL);
	i++;
	threads[3] = CreateThread(NULL, 0, aimbot, &i, 0, NULL);
	i++;
	threads[4] = CreateThread(NULL, 0, bspupdate, &i, 0, NULL);
	i++;
	threads[5] = CreateThread(NULL, 0, glow, &i, 0, NULL);

	WaitForMultipleObjects(6, threads, TRUE, INFINITE);

	return 0;
}

#pragma endregion Main