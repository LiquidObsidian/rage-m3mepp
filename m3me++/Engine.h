/* -*- C++ -*- */

#ifndef ENGINE_H
#define ENGINE_H

#include <windows.h>
#include "Memory.h"

class Engine
{
public:
	Memory Mem;
	DWORD dwClient;
	DWORD dwEngine;
	Offsets Offset;

	Engine(Memory _Mem)
	{
		Mem = _Mem;
		dwClient = Mem.GetModule(L"client.dll").dwBase;
		dwEngine = Mem.GetModule(L"engine.dll").dwBase;
	};

	~Engine() {};
	
	fEngine::
	
	Angle GetBonePosition(DWORD pTargetPlayer, int pBoneID);
	
	float Engine::_NormalizeAngle(float pAngle);
	
	float AngleDifference(Engine::Angle pAngleA, Engine::Angle pAngleB);
	
	Angle NormalizeAngle(Angle pAngle);
	
	Angle GetOrigin();
	
	Angle GetPunch();
	
	Angle GetAngles();

	void SetAngles(Angle pAngle);
	
	Angle mAngle(float x, float y, float z);
	
	Angle GetSmooth(Angle pOriginal, Angle pDest, float Smoothness);
	
	Angle CalcAngle(Angle pSource, Angle pDestination);
};

#endif