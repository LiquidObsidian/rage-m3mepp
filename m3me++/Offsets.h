/*
*	meme offsets
*/

#ifndef OFFSETS_H
#define OFFSETS_H

#include <Windows.h>

class Offsets
{
public:
	DWORD dwSpotted = 0x935;
	DWORD dwSensitivity = 0xA9EC64;
	DWORD dwGlowIndex = 0x86B0;
	DWORD dwGlowObject = 0x4B528D4;
	DWORD dwFlashAlpha = 0x8694;
	DWORD dwBoneMatrix = 0xA78;
	DWORD dwViewOffset = 0x104;
	DWORD dwVecOrigin = 0x134;
	DWORD dwEngine = 0x5D3224;
	DWORD dwViewAngles = 0x4CE0;
	DWORD dwEyeAngles = 0x8C98;
	DWORD dwVecPunch = 0x13E8;
	DWORD dwLocalPlayer = 0xA9948C;
	DWORD dwEntityList = 0x4A3BB64;
	DWORD dwInCross = 0x8CF4;
	DWORD dwTeam = 0xF0;
	DWORD dwAttack = 0x2EADA78;
	DWORD dwJump = 0x4AD0374;
	DWORD dwFlags = 0x100;
	DWORD dwDormant = 0xE9;
	DWORD dwHealth = 0xFC;
	DWORD dwInGame = 0xE8;
	DWORD dwMapName = 0x26C;
};

#endif