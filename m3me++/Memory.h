/*
*	m3mer procmem
*/

#ifndef MEMORY_H
#define MEMORY_H

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <TlHelp32.h>

struct Module {
	DWORD dwBase;
	DWORD dwSize;
};

class Memory {
public:
	Memory();
	~Memory();

	HANDLE _proc;
	DWORD _procID;

	bool Attach(WCHAR* name);

	Module GetModule(WCHAR* name);

	template <class type> type Read(DWORD address, bool isarray = false, int count = 1, type src = 0)
	{
		if (!isarray)
		{
			type read;
			ReadProcessMemory(_proc, (LPVOID)address, &read, sizeof(type), 0);
			return read;
		}
	}

	template <class type> type ReadNew(DWORD address)
	{
		type read;
		ReadProcessMemory(_proc, (LPVOID)address, &read, sizeof(type), NULL);
		return read;
	}

	template <typename type> type Write(DWORD address, type data, bool isarray = false, int count = 1)
	{
		if (!isarray)
		{
			WriteProcessMemory(_proc, (LPVOID)address, &data, sizeof(type)* count, 0);
			return data;
		}
	}

	template <class type> void WriteNew(DWORD address, type data)
	{
		WriteProcessMemory(_proc, (LPVOID)address, &data, sizeof(type), NULL);
	}
};

#endif