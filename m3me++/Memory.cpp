/*
*	m3mer procmem
*/

#include "Memory.h"

Memory::Memory() {}
Memory::~Memory() {}

bool Memory::Attach(WCHAR* name)
{
	HANDLE handle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	PROCESSENTRY32 entry;

	entry.dwSize = sizeof(entry);

	do
	if (!wcscmp(entry.szExeFile, name))
	{
		_procID = entry.th32ProcessID;
		CloseHandle(handle);

		_proc = OpenProcess(PROCESS_ALL_ACCESS, false, _procID);

		return true;
	}
	while (Process32Next(handle, &entry));

	return false;
}

Module Memory::GetModule(WCHAR* name)
{
	HANDLE module = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, _procID);
	MODULEENTRY32 entry;
	entry.dwSize = sizeof(entry);

	do
	if (!wcscmp(entry.szModule, name))
	{
		CloseHandle(module);

		Module mod = { (DWORD)entry.hModule, entry.modBaseSize };
		return mod;
	}
	while (Module32Next(module, &entry));

	Module nullModule = { (DWORD)false, (DWORD)false };

	return nullModule;
}